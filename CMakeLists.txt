cmake_minimum_required(VERSION 2.8.8)
project(Oook)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

find_package(Qt5 COMPONENTS Core Gui Widgets)
find_package(Boost COMPONENTS filesystem system)
find_package(Threads REQUIRED)
find_package(Tinyxml2 REQUIRED)
find_package(Cppdb REQUIRED)
find_package(Fcgi++)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include_directories(${CMAKE_CURRENT_BINARY_DIR} src src/config src/data src/feed src/renderer src/table src/utils) 

add_definitions("-std=c++11 -Ofast -Wall -pedantic")

add_library(liboook ${LIBOOOK_OBJECTS}
    src/constants.cpp 
    src/config/config.cpp 
    src/config/configparser.cpp 
    src/renderer/renderer.cpp
    src/data/data.cpp
    src/data/metadata.cpp 
    src/data/author.cpp 
    src/data/book.cpp 
    src/data/download.cpp 
    src/data/extension.cpp 
    src/data/generic.cpp 
    src/data/language.cpp 
    src/data/publisher.cpp 
    src/data/rating.cpp
    src/data/serie.cpp 
    src/data/tag.cpp
    src/table/table.cpp
    src/table/metadatatable.cpp 
    src/table/booktable.cpp
    src/table/authortable.cpp
    src/table/publishertable.cpp 
    src/table/serietable.cpp 
    src/table/tagtable.cpp 
    src/utils/utils.cpp 
    src/utils/mimeutils.cpp
    src/feed/acquisitionfeed.cpp
    src/feed/navigationfeed.cpp
    src/feed/feed.cpp
)

target_link_libraries(liboook ${Cppdb_LIBRARIES} ${Tinyxml2_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

if(${Fcgi++_FOUND})
    include_directories(${Fcgi++_INCLUDE_DIRS})

    add_executable(search.cgi src/search.cpp ${LIBOOOK_OBJECTS})
    target_link_libraries(search.cgi liboook ${Fcgi++_LIBRARIES})

    install(TARGETS search.cgi RUNTIME DESTINATION bin)
endif()

if(${Qt5_FOUND} AND ${Boost_FOUND})
    set(CMAKE_AUTOMOC ON)
    set(CMAKE_AUTOUIC ON)
    
    include_directories(${Boost_INCLUDE_DIRS} ${Qt5Widgets_INCLUDES} ${Qt5Core_INCLUDES} ${Qt5Gui_INCLUDES} ${Qt5Svg_INCLUDES})
    add_definitions(${Qt5Widgets_DEFINITIONS})
    
    add_executable(oook src/config/configdialog.cpp src/installer.cpp src/utils/filesystemutils.cpp ${LIBOOOK_OBJECTS})
    target_link_libraries(oook liboook ${Boost_LIBRARIES} Qt5::Core Qt5::Gui Qt5::Widgets)
    
    install(TARGETS oook RUNTIME DESTINATION bin)
endif()
