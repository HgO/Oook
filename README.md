# Introduction

Oook (Open Orang-outan Knowledge) is a light OPDS library server written in C++. 
The main purpose of this application is to be installed on light or small devices, such as a RaspberryPi or a PirateBox, which is a small router.

The software is divided in two main parts: 
- Firstly, we read a Calibre database for generating HTML files. These files will be used for displaying the catalog containing books, authors, etc.
- Then, we provide a search engine using FastCGI for the catalog. This allows visitors to look after some specific books quite easily, without using too much ressources on the server.

This means that, except for the search engine, the catalog is static and contains only HTML webpages. Ditto for OPDS catolog, with XML pages.

[//]: # (This also means that it doesn't need much config to use the library, and you can just put the catalog on a USB device and open it with a browser.)

Besides, if you want to use the search engines, you'll need some server configuration, such as enabling FastCGI.

# Installation

## Requirements

In order to build this software, you'll need the following libraries installed:

- Sqlite3
- [CppDB](http://cppcms.com/sql/cppdb/)
- [TinyXML2](http://www.grinninglizard.com/tinyxml2/)

For generating the static catalog (HTML and XML pages), you'll also need:

- Qt5
- Boost

For the search engine, you'll need:

- FastCGI

For instance, on OpenSUSE, you will have to run this command:

```shell
zypper install cppdb-devel libcppdb_sqlite3-0 tinyxml2-devel libqt5-qtbase-devel boost-devel fastcgi-devel
```

## Build

For building this application, all you need is to run these commands: 

```shell
mkdir build
cd build
cmake ..
make
```

## CSS and Javascript

In order to retrieve the boostrap CSS and Javascript used in HTML pages, you must run `bower update` in the `template/` directory.

Please refer to [http://bower.io/](http://bower.io/) for `bower`'s installation. 

# Usage

Once the software has been installed, just run `./oook` in the `build/` directory. 

This will show a dialog window asking you to configure the software. 

Amongst other things, you'll be asked to choose the folder containing your Calibre library, and optionally the location of your Calibre database. 

Then, choose the folder where you want to generate your web library, and the hostname on which your catalog will be. This last step is needed for the [OpenSearch](http://www.opensearch.org/Home)'s configuration.

# Configuration

Now that your catalog is generated and uploaded on your HTTP server, you need to configure it.

First, you must install the FastCGI module of your favorite HTTP server.

## Apache2

Here is a sample configuration on an Apache2 server:

```shell
<Directory /path/to/your/catalog/>
  AllowOverride AuthConfig FileInfo
   Order allow,deny
  Allow from all
  
  Options +ExecCGI -Includes
  AddHandler fcgid-script .cgi
  
  DirectoryIndex index.html index.xml search.cgi
</Directory>
```

## Nginx

Here is a sample configuration on a Nginx server:

```shell
server {
    listen 80;

    # hostname
    server_name example.com;

    # logs
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    root /path/to/your/catalog/;

    # this is needed for having pretty urls
    index index.html index.xml;

    
    # FastCGI configuration
    location ~ (/search/|\.cgi)$  {
        fastcgi_pass 127.0.0.1:9000;

        include /etc/nginx/fastcgi_params;
        
        fastcgi_index search.cgi;
    }
}
```

Then, you must run the following command in the `/path/to/your/catalog/search/` directory:

```shell
spawn-fcgi -p 9000 search.cgi
```

This will launch the search engine.


# Customizing

I tried to make this software as flexible as possible. 

So, if you want to change the display of web pages, you can easily modify the templates used to generate these pages. 
All you need to do is going to the `template/` folder and edit the `*.tpl.html` files.