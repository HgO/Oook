#include "book.hpp"

#include <algorithm>

#include "author.hpp"
#include "tag.hpp"
#include "publisher.hpp"
#include "serie.hpp"
#include "download.hpp"
#include "rating.hpp"
#include "language.hpp"

#include "utils.hpp"
#include "mimeutils.hpp"
#include "config.hpp"
#include "renderer.hpp"

#include "feed.hpp"

Book::Book(int id, std::string const& name, std::string const& sort) :
    Data(id, name, sort),
    _index(),
    _summary(),
    _path(),
    _rating(nullptr),
    _serie(nullptr),
    _authors(),
    _tags(),
    _publishers(),
    _formats(),
    _language()
{
}

void Book::addAuthor(Author* author) {
    _authors.insert(author);
}

void Book::addTag(Tag* tag) {
    _tags.insert(tag);
}

void Book::addPublisher(Publisher* pub) {
    _publishers.insert(pub);
}

void Book::setSerie(Serie* serie) {
    _serie = serie;
}

void Book::addFormat(Download* format) {
    _formats.insert(format);
}

void Book::addLanguage(Language* language) {
    _language.insert(language);
}

void Book::setRating(Rating* rating) {
    _rating = rating;
}

Book* Book::create(
    int id, 
    const std::string& name, 
    const std::string& sort,
    int index,
    std::string const& summary,
    std::string const& path
)
{   
    Book* book = Factory::create<Book>(id, name, sort);
    if(index != 0)
        book->_index = index;
    
    if(!summary.empty())
        book->_summary = summary;
    if(!path.empty())
        book->_path = path;
        
    return book;
}

std::string Book::renderLink() const
{
    return "";
}

std::string Book::renderItem() const
{
    std::string out = Renderer::instance().open("item.book.tpl.html");
    
    Utils::replaceAll(out, "{{id}}", std::to_string(_id));
    Utils::replaceAll(out, "{{title}}", Utils::encodeHTML((!_name.empty()) ? _name : "Unknown")); 
    Utils::replaceAll(out, "{{url}}", "{{root}}" + url());
    Utils::replaceAll(out, "{{legend}}", Utils::encodeHTML(Utils::join(" & ", _authors)));
    
    return out;
}

std::string Book::render(std::size_t ) const
{
    std::string downloads;
    for(Data const* format: _formats)
        downloads += format->renderLink();
    
    std::string tags;
    for(auto it = _tags.begin(); it != _tags.end(); ++it) {
        if(it != _tags.begin())
            tags += ", ";
        tags += (*it)->renderLink();
    }
    
    std::string pubs;
    for(auto it = _publishers.begin(); it != _publishers.end(); ++it) {
        if(it != _publishers.begin())
            pubs += ", ";
        pubs += (*it)->renderLink();
    }
        
    std::string authors;
    for(auto it = _authors.begin(); it != _authors.end(); ++it) {
        if(it != _authors.begin())
            authors += " & ";
        authors += (*it)->renderLink();
    }
    
    std::string series = _serie->renderLink();
    
    if(!series.empty())
        series = Utils::ordinal(_index) + " book in the " + series + "'s series";
    
    std::string out = Renderer::instance().open("book.tpl.html");
    
    int rating = static_cast<Rating*>(_rating)->rating() / 2;
    
    Utils::replaceAll(out, "{{downloads}}", downloads);
    Utils::replaceAll(out, "{{tags}}", tags);
    Utils::replaceAll(out, "{{authors}}", authors);
    Utils::replaceAll(out, "{{publishers}}", pubs);
    Utils::replaceAll(out, "{{series}}", series);
    Utils::replaceAll(out, "{{summary}}", _summary);
    Utils::replaceAll(out, "{{rating-1}}", (rating > 0) ? "" : "-empty");
    Utils::replaceAll(out, "{{rating-2}}", (rating > 1) ? "" : "-empty");
    Utils::replaceAll(out, "{{rating-3}}", (rating > 2) ? "" : "-empty");
    Utils::replaceAll(out, "{{rating-4}}", (rating > 3) ? "" : "-empty");
    Utils::replaceAll(out, "{{rating-5}}", (rating > 4) ? "" : "-empty");
    Utils::replaceAll(out, "{{name}}", _name);
    
    return out;
}

XMLNode* Book::generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData) const
{
    XMLElement* eEntry = feed->addEntry(this, parent);
    
    feed->addLink(feed->dir() + url() + "thumbnail.jpg", "", "http://opds-spec.org/image/thumbnail", "image/jpeg", eEntry);
    feed->addLink(feed->dir() + url() + "cover.jpg", "", "http://opds-spec.org/image", "image/jpeg", eEntry);
        
    feed->addAuthor(Utils::join(" &amp; ", _authors), "", eEntry);
        
    feed->addSummary(_summary, eEntry);
    
    for(Data const* format: _formats) 
        feed->addLink(
                feed->dir() + url() + Utils::encodeURI(format->url()), 
                "", 
                "http://opds-spec.org/acquisition", 
                MimeUtils::get(static_cast<Download const*>(format)->format()), 
                eEntry
            );
    
        
    for(Data const* author: _authors)
        if(parentData != author)
            feed->addAcquisitionLink(feed->opdsDir() + author->url(), "Livres de " + author->name(), "related", eEntry);
            
    if(parentData != _serie and !_serie->name().empty())
        feed->addAcquisitionLink(feed->opdsDir() + _serie->url(), "Livres dans la série " + _serie->name(), "related", eEntry);
    
    for(Data const* publisher: _publishers)
        if(!publisher->name().empty())
        {
            if(parentData != publisher)
                feed->addAcquisitionLink(feed->opdsDir() + publisher->url(), "Livres de l'éditeur " + publisher->name(), "related", eEntry);
            else
                feed->addPublisher(publisher->name(), eEntry);
        }
        
    feed->addLink(feed->dir() + url(), "Voir sur le site", "alternate", "text/html", eEntry);
        
    for(Data const* tag: _tags)
        if(!tag->name().empty())
            feed->addCategory(tag->name(), eEntry);
    
    return eEntry;
}
