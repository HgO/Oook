#ifndef __LANGUAGE_H__
#define __LANGUAGE_H__

#include "metadata.hpp"

class Language : public Metadata {
    friend struct Factory;
public:
    virtual ~Language() {}
    
    virtual std::string type() const override {
        return "language";
    }
    
    static Language* create(int id, std::string const& name);
protected:
    Language(int id, std::string const& name, std::string const& sort = "") : Metadata(id, name) {}
};

#endif