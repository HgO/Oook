#ifndef __DATA_H__
#define __DATA_H__

#include <tinyxml2.h>
#include <unordered_map>
#include <utility>
#include <string>

using namespace tinyxml2;

class Data;
class Feed;

struct Factory {
    template<class T, typename K = int>
    static T* create(K id, std::string const& name, std::string const& sort = "")
    {
        static std::unordered_map<K, T*> map;
        
        auto it = map.find(id);
        
        if(it == map.end())
            it = map.insert(std::pair<K, T*>(id, new T(id, name, sort))).first;
        
        return it->second;
    }
};

namespace std
{
    template<>
    struct less<Data*>
    {
    public:
        bool operator()(Data const* left, Data const* right) const;
    };
}

class Data
{
public:
    virtual ~Data() {}
    
    int id() const
    {
        return _id;
    }
    
    std::string name() const 
    {
        return _name;
    }
    
    std::string sort() const
    {
        return _sort;
    }
    
    std::string description() const 
    {
        return _description;
    }
    
    virtual std::string url() const 
    {
        if(_id >= 0)
            return type() + "/" + std::to_string(_id) + "/";
        else {
            std::string url = type();
            
            if(!url.empty())
                url += "/";
            
            return url;
        }
    }
    
    virtual std::string type() const = 0;
    
    virtual bool compare(Data const* other) const {
        return _lowerSort.compare(other->_lowerSort) < 0;
    }
    
    virtual std::string renderLink() const;
    virtual std::string renderItem() const = 0;
    virtual std::string render(std::size_t page = 0) const = 0;
    
    virtual XMLNode* generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData = nullptr) const = 0;
    virtual Feed* createFeed(Feed const* parent = nullptr, std::size_t page = 0) const = 0;
protected:
    Data(int id, const std::string& name, std::string const& sort = "");
  
    int _id;
    std::string _name;
    std::string _sort;
    std::string _lowerSort;
    std::string _description;
};

#endif