#include "language.hpp"

Language* Language::create(int id, const std::string& name)
{
    return Factory::create<Language>(id, name);
}
