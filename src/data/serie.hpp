#ifndef __SERIE_H__
#define __SERIE_H__

#include "metadata.hpp"

#include <string>

struct comparator_series {
    bool operator() (Data const* data, Data const* other);
};

class Serie : public Metadata {
    friend struct Factory;
public:
    virtual ~Serie() {}
    
    virtual std::string type() const override {
        return "series";
    }
    
    static Serie* create(int id, std::string const& name, std::string const& sort);
    
    virtual std::string renderItems(std::size_t min, std::size_t max) const override
    {
        std::set<Data*, comparator_series> dataset(_dataset.begin(), _dataset.end());
        return Metadata::renderItems(dataset, min, max);
    }
    virtual XMLNode* generateFeed(
        Feed* feed, 
        std::size_t page, 
        XMLNode* parent, 
        Data const* parentData
    ) const override {
        std::set<Data*, comparator_series> dataset(_dataset.begin(), _dataset.end());
        return Metadata::generateFeed(dataset, feed, page, parent, parentData);
    }
protected:
    Serie(int id, std::string const& name, std::string const& sort) : Metadata(id, name, sort) {}
};

#endif