#ifndef __METADATA_H__
#define __METADATA_H__

#include "data.hpp"

#include <string>
#include <set>
#include <tinyxml2.h>

#include "config.hpp"
#include "feed.hpp"

using namespace tinyxml2;

class Book;
class Feed;

class Metadata : public Data {
public:
    virtual ~Metadata() {}
    
    std::set<Data*> const& dataset() const 
    {
        return _dataset;
    }
    
    void dataset(std::set<Data*> const& dataset) 
    {
        _dataset = dataset;
    }
    
    bool isDatasetEmpty() const
    {
        return _dataset.empty() or (_dataset.size() == 1 and (*_dataset.begin())->name().empty());
    }
    
    void addData(Data* data) 
    {
        _dataset.insert(data);
    }
    
    void addDataset(std::set<Data*> const& data) 
    {
        _dataset.insert(data.begin(), data.end());
    }
    
    template<typename C>
    std::string renderItems(std::set<Data*, C> const& dataset, std::size_t min, std::size_t max) const
    {
        std::string items;
        
        if(min < dataset.size())
        {
            typename std::set<Data*, C>::const_iterator it = std::next(dataset.begin(), min);
            while(it != dataset.end() and min < max)
            {
                items += (*it)->renderItem();
                ++min; ++it;
            }
        }
        
        return items;
    }
    virtual std::string renderItems(std::size_t min, std::size_t max) const;
    
    virtual std::string renderItem() const override;
    virtual std::string render(std::size_t page = 0) const override;
    
    template<typename C>
    XMLNode* generateFeed(
        std::set<Data*, C> const& dataset, 
        Feed* feed, 
        std::size_t page, 
        XMLNode* parent, 
        Data const* parentData
    ) const {
        std::size_t offset = std::stoi(Config::get("items_per_page"));
        std::size_t min = offset*page;
        std::size_t max = min + offset;
        
        feed->addOpensearch("totalResults", dataset.size(), parent);
        feed->addOpensearch("itemsPerPage", offset, parent);
        
        if(min < dataset.size())
        {
            typename std::set<Data*, C>::const_iterator it = std::next(dataset.begin(), min);
            while(it != dataset.end() and min < max)
            {
                (*it)->generateFeed(feed, page, parent, this);
                ++min; ++it;
            }
        }
        
        return parent;
    }
    virtual XMLNode* generateFeed(
        Feed* feed, 
        std::size_t page, 
        XMLNode* parent, 
        Data const* parentData = nullptr
    ) const override;
    virtual Feed* createFeed(Feed const* parent = nullptr, std::size_t page = 0) const override;
protected:
    Metadata(int id, std::string const& name, std::string const&sort = "") : Data(id, name, sort) {};
    
    std::set<Data*> _dataset;
};
#endif