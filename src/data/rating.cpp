#include "rating.hpp"

#include "utils.hpp"

Rating* Rating::create(int id, int rating)
{   
    Rating* dRating = Factory::create<Rating>(id, (rating > 0) ? std::to_string(rating / 2) + " " + Utils::plural("star", rating / 2) : "");
    dRating->_rating = rating;
    
    return dRating;
}