#ifndef __RATING_H__
#define __RATING_H__

#include "metadata.hpp"

#include <string>

class Rating : public Metadata {
    friend struct Factory;
public:
    virtual ~Rating() {}
    
    virtual std::string type() const override {
        return "rating";
    }
    
    int rating() const {
        return _rating;
    }
    
    static Rating* create(int id, int rating);
protected:
    Rating(int id, std::string const& name, std::string const& sort) : Metadata(id, name), _rating(-1) {}
protected:
    int _rating;
};

#endif