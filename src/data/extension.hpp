#ifndef __EXTENSION_H__
#define __EXTENSION_H__

#include "metadata.hpp"

#include <string>

class Extension : public Metadata {
    friend struct Factory;
public:
    virtual ~Extension() {}
    
    virtual std::string type() const override {
        return "format";
    }
    
    static Extension* create(std::string const& name);
protected:
    Extension(int id, std::string const& name, std::string const& sort) : Metadata(id, name) {}
};

#endif