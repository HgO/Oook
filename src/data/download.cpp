#include "download.hpp"

#include <algorithm>

#include "utils.hpp"
#include "config.hpp"
#include "renderer.hpp"

Download::Download(int id, std::string const& name, std::string const& format, int size) :
    Metadata(id, name, std::to_string(id) + "." + format),
    _format(format),
    _size(size)
{
    std::transform(_format.begin(), _format.end(), _format.begin(), tolower);
}

std::string Download::renderLink() const
{
    if(_name.empty())
        return "";
    
    std::string content = Renderer::instance().open("link.download.tpl.html");
    
    Utils::replaceAll(content, "{{url}}", Utils::encodeURI(url()));
    Utils::replaceAll(content, "{{format}}", _format);
        
    return content;
}

Download* Download::create(int id, const std::string& name, const std::string& format, int size)
{
    Download* obj = Factory::create<Download>(id, name, format); 
    obj->_size = size;
    
    return obj;
}
