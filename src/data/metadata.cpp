#include "metadata.hpp"

#include <cmath>

#include "book.hpp"
#include "utils.hpp"
#include "config.hpp"
#include "renderer.hpp"

#include "feed.hpp"
#include "acquisitionfeed.hpp"

std::string Metadata::renderItem() const
{
    std::string item = Renderer::instance().open("item.tpl.html");
    
    std::string name = _sort;
    if(_id < 0)
        name = _name;
    else if(_sort.empty())
        name = "Unknown";
    
    Utils::replaceAll(item, "{{title}}", Utils::encodeHTML(name));
    Utils::replaceAll(item, "{{id}}", std::to_string(_id));
    Utils::replaceAll(item, "{{url}}", "{{root}}" + url());
    Utils::replaceAll(item, "{{image}}", "{{root}}" + type() + "/icon.png");
    Utils::replaceAll(item, "{{count}}", (_dataset.size() > 0) ? std::to_string(_dataset.size()) : "");
    
    return item;
}

std::string Metadata::renderItems(std::size_t min, std::size_t max) const
{
    return renderItems(_dataset, min, max);
}

std::string Metadata::render(std::size_t page) const
{
    std::size_t offset = std::stoi(Config::get("items_per_page"));
    std::size_t min = offset*page;
    std::size_t max = min + offset;
    
    std::string section = Renderer::instance().open("section.tpl.html");
    
    Utils::replaceAll(section, "{{id}}", type());
    Utils::replaceAll(section, "{{title}}", _name);
    Utils::replaceAll(section, "{{items}}", renderItems(min, max));
    
    return section;
}

XMLNode* Metadata::generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData) const 
{
    return generateFeed(_dataset, feed, page, parent, parentData);
}

Feed* Metadata::createFeed(Feed const* parent, std::size_t page) const
{
    return new AcquisitionFeed(this, page, parent);
}