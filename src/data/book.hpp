#ifndef __BOOK_H__
#define __BOOK_H__

#include <map>
#include <set>
#include <string>

#include "utils.hpp"
#include "data.hpp"

class Author;
class Tag;
class Publisher;
class Serie;
class Download;
class Rating;
class Language;

class Book : public Data {
    friend struct Factory;
public:
    virtual ~Book() {}
    
    int index() const {
        return _index;
    }
    
    std::string const& summary() const {
        return _summary;
    }
    
    Data* rating() const {
        return _rating;
    }
    
    std::string const& path() const {
        return _path;
    }
    
    virtual std::string url() const override {
        return type() + "/" + std::to_string(_id) + "/";
    }
    
    std::set<Data*> const& authors() const {
        return _authors;
    }
    
    std::set<Data*> const& tags() const {
        return _tags;
    }
    
    std::set<Data*> const& publishers() const {
        return _publishers;
    }
    
    Data* serie() const {
        return _serie;
    }
    
    std::set<Data*> const& formats() const {
        return _formats;
    }
    
    std::set<Data*> const& languages() const {
        return _language;
    }
    
    virtual std::string type() const override {
        return "book";
    }
    
    void addAuthor(Author* author);
    void addTag(Tag* tag);
    void addPublisher(Publisher* pub);
    void setSerie(Serie* serie);
    void addFormat(Download* format);
    void addLanguage(Language* language);
    
    void setRating(Rating* rating);
    
    static Book* create(
        int id, 
        std::string const& name, 
        std::string const& sort,
        int index,
        std::string const& summary = std::string(),
        std::string const& path = std::string()
    );
    
    virtual std::string renderLink() const override;
    virtual std::string renderItem() const override;
    virtual std::string render(std::size_t page = 0) const override;
    
    virtual XMLNode* generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData = nullptr) const override;
    virtual Feed* createFeed(Feed const* parent = nullptr, std::size_t page = 0) const override { return nullptr; }
protected:
    Book(int id, std::string const& name, std::string const& sort);
    
    int _index;
    std::string _summary;
    std::string _path;
    Data* _rating;
    Data* _serie;
    std::set<Data*> _authors;
    std::set<Data*> _tags;
    std::set<Data*> _publishers;
    std::set<Data*> _formats;
    std::set<Data*> _language;
};

#endif