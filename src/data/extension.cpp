#include "extension.hpp"

Extension* Extension::create(const std::string& name)
{
    static std::unordered_map<std::string, int> ids;
    static int id = 0;
    
    auto it = ids.find(name);
    if(it == ids.end())
        it = ids.insert(std::pair<std::string, int>(name, id++)).first;
    
    return Factory::create<Extension>(it->second, name);
}
