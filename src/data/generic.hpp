#ifndef __HOMEDATA_H__
#define __HOMEDATA_H__

#include "metadata.hpp"

class Generic : public Metadata {
    friend struct Factory;
public:
    virtual ~Generic() {}
    
    virtual std::string type() const override {
        return _type;
    }
    
    virtual XMLNode* generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData = nullptr) const override;
    virtual Feed* createFeed(Feed const* parent = nullptr, std::size_t page = 0) const override;
    
    static Generic* create(std::string const& type, std::string const& description = "", std::string const& name = "", std::string const& sort = "");
protected:
    Generic(int id, std::string const& name, std::string const& sort) : Metadata(id, name, sort) {}
protected:
    std::string _type;
};

#endif