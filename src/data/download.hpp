#ifndef __FORMAT_H__
#define __FORMAT_H__

#include <string>

#include "metadata.hpp"
#include "utils.hpp"

class Download : public Metadata {
    friend struct Factory;
public:
    virtual ~Download() {}
    
    std::string format() const {
        return _format;
    }
    
    int size() const {
        return _size;
    }
    
    virtual std::string url() const override {
        return _name + "." + _format;
    }
    
    virtual std::string type() const override {
        return "format";
    }
    
    virtual std::string renderLink() const override;
    
    static Download* create(int id, std::string const& name, std::string const& format, int size = 0);
protected:
    Download(int id, std::string const& name, std::string const& format, int size = 0);
    
    std::string _format;
    int _size;
};

#endif