#include "data.hpp"

#include <algorithm>

#include "utils.hpp"
#include "renderer.hpp"

bool std::less<Data*>::operator()(Data const* left, Data const* right) const {
    return left->compare(right);
}

Data::Data(int id, std::string const& name, std::string const& sort) :
    _id(id),
    _name(name),
    _sort(sort.empty() ? name : sort),
    _lowerSort(_sort),
    _description()
{
    std::transform(_lowerSort.begin(), _lowerSort.end(), _lowerSort.begin(), tolower);
}

std::string Data::renderLink() const
{
    if(_name.empty())
        return "";
    
    std::string link = Renderer::instance().open("link.tpl.html");
    
    Utils::replaceAll(link, "{{name}}", _name);
    Utils::replaceAll(link, "{{url}}", "../../" + url());
    
    return link;
}