#include "generic.hpp"

#include "feed.hpp"
#include "navigationfeed.hpp"
#include "acquisitionfeed.hpp"
#include "config.hpp"

Generic* Generic::create(const std::string& type, std::string const& description, std::string const& name, std::string const& sort)
{
    static std::unordered_map<std::string, int> ids;
    static int id = 0;
    
    auto it = ids.find(name);
    if(it == ids.end())
        it = ids.insert(std::pair<std::string, int>(name, --id)).first;
    
    Generic* generic = Factory::create<Generic>(it->second, name, sort);
    generic->_type = type;
    if(!description.empty())
        generic->_description = description;
    
    return generic;
}

XMLNode* Generic::generateFeed(Feed* feed, std::size_t page, XMLNode* parent, Data const* parentData) const
{
    if(_type == "book")
        return Metadata::generateFeed(feed, page, parent);
    
    std::size_t offset = std::stoi(Config::get("items_per_page"));
    std::size_t min = offset*page;
    std::size_t max = min + offset;
    
    if(min < _dataset.size())
    {
        std::set<Data*>::const_iterator it = std::next(_dataset.begin(), min);
        while(it != _dataset.end() and min < max)
        {
            Data* data = *it;
            
            if(data->type() != "search")
            {
                XMLElement* eEntry = feed->addEntry(data, parent);
                feed->addNavigationLink(data, eEntry);
            }
            
            ++min; ++it;
        }
    }
    
    return parent;
}

Feed* Generic::createFeed(Feed const* parent, std::size_t page) const
{
    if(_type != "book")
        return new NavigationFeed(this, page, parent);
    else
        return new AcquisitionFeed(this, page, parent);
}
