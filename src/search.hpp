#ifndef __SEARCH_H__
#define __SEARCH_H__

#include <vector>
#include <string>
#include <set>

class Table;
class Data;
class Generic;

struct SearchQuery {
    std::set<std::string> ands;
    std::set<std::string> ors;
    std::set<std::string> nots;  
};

enum SearchState {
    None = ' ',
    DoubleQuote = '"',
    BackSlash = '\\'
};

class Search {
public:
    Search();
    ~Search();
    
    Generic* search(std::string const& entry, bool deepSearch = true);
    std::vector<std::string> tokenize(std::string const& entry);
    SearchQuery group(const std::vector< std::string >& tokens);
protected:
    Table* _tables[5];
};

#endif