#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <string>

extern const std::string SOFTWARE_NAME;
extern const std::string SOFTWARE_VERSION;
extern const std::string SOFTWARE_WEBSITE;
extern const std::string SOFTWARE_SEARCH;

#endif