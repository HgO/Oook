#include "installer.hpp"

#include <fstream>

#include <boost/filesystem.hpp>

#include <QtCore>
#include <QtGui>
#include <QApplication>
#include <QGridLayout>
#include <QMimeDatabase>
#include <cmath>

#include "config.hpp"
#include "configparser.hpp"
#include "utils.hpp"
#include "data.hpp"
#include "book.hpp"
#include "author.hpp"
#include "serie.hpp"
#include "download.hpp"
#include "extension.hpp"
#include "generic.hpp"

#include "booktable.hpp"
#include "renderer.hpp"

#include "filesystemutils.hpp"
#include "constants.hpp"
#include "mimeutils.hpp"

#include "navigationfeed.hpp"
#include "acquisitionfeed.hpp"

Installer::Installer(QDialog* parent) :
_booktable(),
_i(0),
_nformats(0),
_spinner(parent ? (new QProgressDialog("Installation en cours...", "", 0, 0, parent)) : nullptr)
{
    if(_spinner)
    {
        _spinner->setCancelButton(nullptr);
        _spinner->setValue(0);
        _spinner->setFixedSize(_spinner->size());
        _spinner->setModal(true);
    }
    
    QMimeDatabase mimeDatabase;
    
    // First, we create the destination directory if it doesn't exist yet
    createDirectory(Config::get("catalog"));
    
    Generic* home = Generic::create("", "Racine du catalogue " + Config::get("title"));
    Generic* authors = Generic::create("author", "Tous les livres triés par auteurs", "Auteurs");
    Generic* books = Generic::create("book", "Tous les livres triés par titre", "Livres");
    Generic* formats = Generic::create("format", "Tous les livres triés par format de fichier", "Formats");
    Generic* languages = Generic::create("language", "Tous les livres triés par langue", "Langues");
    Generic* publishers = Generic::create("publisher", "Tous les livres triés par maisons d'édition", "Éditeurs");
    Generic* ratings = Generic::create("rating", "Tous les livres triés par notes", "Classement");
    Generic* series = Generic::create("series", "Tous les livres triés par séries", "Séries");
    Generic* tags = Generic::create("tag", "Tous les livres triés par tags", "Tags");
    Generic* search = Generic::create("search", "Effectue une recherche dans le catalogue", "Recherche", "Search");
    
    books->dataset(_booktable.fetchAll());
    for(Data const* data: books->dataset()) 
    {
        // For each book, we retrieve metadata related to the book
        Book* book = static_cast<Book*>(_booktable.fetch(data->id()));
        
        authors->addDataset(book->authors());
        languages->addDataset(book->languages());
        publishers->addDataset(book->publishers());
        ratings->addData(book->rating());
        series->addData(book->serie());
        tags->addDataset(book->tags());
        
        for(Data const* data: book->formats())
        {
            Download const* download = static_cast<Download const*>(data);
            
            std::string path = Config::get("calibre") + book->path() + "/" + download->name() + "." + download->format();
            QMimeType mime = mimeDatabase.mimeTypeForFile(path.c_str());
            
            MimeUtils::put(download->format(), mime.name().toStdString());
            
            Extension* extension = Extension::create(Utils::titlize(download->format()));
            extension->addData(book);
            
            formats->addData(extension);            
        }
        
        _nformats += book->formats().size();
    }
    
    MimeUtils::save(Config::get("catalog") + "mimetypes.txt");
    
    home->addData(authors);
    home->addData(books);
    home->addData(formats);
    if(!languages->isDatasetEmpty())
        home->addData(languages);
    home->addData(publishers);
    if(!ratings->isDatasetEmpty())
        home->addData(ratings);
    home->addData(series);
    home->addData(tags);
    home->addData(search);
    
    if(_spinner)
        _spinner->setRange(0, _nformats);
    
    // Create HTML pages, copy ebooks and icons
    create(home);
    
    
    createOpensearch(Config::get("catalog") + "search/opensearch.xml");
    
    
    FileSystemUtils::copyDirectory(Config::get("template"), Config::get("catalog") + "template/", true, false);
    FileSystemUtils::copyDirectory(Config::get("template") + "css/", Config::get("catalog") + "css/", true, true);
    FileSystemUtils::copyDirectory(Config::get("template") + "bower_components/", Config::get("catalog") + "bower_components/", true, true);
    
    FileSystemUtils::copyFile(Config::get("database"), Config::get("catalog") + "metadata.db", true);
    FileSystemUtils::copyFile(".", Config::get("catalog") + "search/", SOFTWARE_SEARCH, true);
    
    std::string catalog = Config::get("catalog");
    std::string items_per_page = Config::get("items_per_page");
    std::string title = Config::get("title");
    
    Config::clear();
    
    Config::set("template", "../template/");
    Config::set("database", "../metadata.db");
    Config::set("items_per_page", items_per_page);
    Config::set("title", title);
    
    ConfigParser().write(catalog + "search/oook.conf");
    
    if(!_spinner)
        std::cout << "Installation terminée: " << _i << "/" << _nformats << " livres copiés." << std::endl;
}

void Installer::create(Data const* data, std::string const& dir, std::string const& pwd, Feed const* parent)
{
    std::string path = Config::get("catalog") + pwd;
    std::string opdsPath = Config::get("catalog") + "opds/" + pwd;
    
    std::string header = Renderer::instance().header(data->type());
    std::string footer = Renderer::instance().footer();
    
    Utils::replaceAll(header, "{{root}}", dir);
    
    std::string filename = "index.html";
    
    std::size_t pages = 1;
    
    createDirectory(path);
    createDirectory(opdsPath);
    
    if(dynamic_cast<Metadata const*>(data))
    {
        Metadata const* meta = static_cast<Metadata const*>(data);
            
        std::size_t offset = std::stoi(Config::get("items_per_page"));
        pages = std::ceil(double(meta->dataset().size()) / offset);
        
        // copy icon
        if(!pwd.empty())
            FileSystemUtils::copyFile(Config::get("template") + "icon/" + data->type() + ".png", path + "icon.png", true);
        
        Feed* feed = data->createFeed(parent, 0);
        
        for(Data const* child: meta->dataset())    
        {
            std::string subpwd = pwd;
            if(child->id() >= 0)
                subpwd += std::to_string(child->id());
            else
                subpwd += child->type();
            subpwd += "/";
            
            std::string subdir = "../" + dir;
            
            if(!dynamic_cast<Book const*>(child) or data->type() == child->type())
                create(child, subdir, subpwd, feed);
        }
        
        std::string feedname = "index.xml";
        std::string prevLink;
        std::string nextLink;
        for(std::size_t page = 0; page < pages; ++page)
        {
            if(page != 0)
            {
                feed->setFirstLink();
                
                if(page-1 != 0)
                    feed->setPrevLink(feedname);
                else
                    feed->setPrevLink();
            }
            
            if(page < pages-1)
            {
                nextLink = std::to_string(page+1) + ".xml";
                
                feed->setNextLink(nextLink);
                
                feed->setLastLink(std::to_string(pages-1) + ".xml");
            }
            
            feed->write(opdsPath + feedname);
            
            delete feed;
            
            if(page < pages)
            {
                feed = data->createFeed(parent, page+1);
            
                prevLink = feedname;
                feedname = nextLink;
            }
        }
    } else
        copy(data, path);
    
    for(std::size_t page = 0; page < pages; ++page)
    {
        if(page != 0)
            filename = std::to_string(page) + ".html";
            
        std::string content = data->render(page) + Renderer::instance().pagination(page, pages);
        Utils::replaceAll(content, "{{root}}", dir);
            
        std::ofstream html(path + filename);
            
        html << header << content << footer;
        
        html.close();
    }
}

void Installer::copy(Data const* data, std::string const& dst) 
{
    std::vector<std::string> mediumSize = Utils::split('x', Config::get("cover_size"));
    std::vector<std::string> thumbnailSize = Utils::split('x', Config::get("thumbnail_size"));
    
    std::size_t mediumWidth = std::stoi(mediumSize.at(0));
    std::size_t mediumHeight = std::stoi(mediumSize.at(1));
    
    std::size_t thumbnailWidth = std::stoi(thumbnailSize.at(0));
    std::size_t thumbnailHeight = std::stoi(thumbnailSize.at(1));
    
    Book const* book = static_cast<Book const*>(data);
        
    std::string src = Config::get("calibre") + "/" + book->path() + "/";
    // copy ebooks
    for(Data const* data: book->formats()) 
    {
        try {
            FileSystemUtils::copyFile(src, dst, data->url(), false);
            
            if(_spinner)
            {
                _spinner->setValue(++_i);
                QApplication::processEvents();
            } else
                std::cout << "Installation en cours... " << std::round(double(++_i) / _nformats * 100) << "%\r" << std::flush;
        } catch(std::exception const& e) {
            std::cerr << e.what() << std::endl;
        }
        
    }
        
    // copy cover
    try {
        FileSystemUtils::copyFile(src, dst, "cover.jpg", false);
            
        QImage cover((dst + "cover.jpg").c_str());
            
        QSize medium_size(mediumWidth, mediumHeight);
        QSize thumbnail_size(thumbnailWidth, thumbnailHeight);
            
        QImage medium = cover.scaled(medium_size, Qt::KeepAspectRatio);
        QImage thumbnail = cover.scaled(thumbnail_size, Qt::KeepAspectRatio);
            
        medium.save((dst + "medium.jpg").c_str()); 
        thumbnail.save((dst + "thumbnail.jpg").c_str());
    } catch(std::exception const& e) { }
}

void Installer::createDirectory(std::string const& dir) const {
    boost::filesystem::create_directory(dir);
    
    if(!boost::filesystem::is_directory(dir))
        throw std::runtime_error("Error: can't create directory " + dir);
}

void Installer::createOpensearch(std::string const& filename) const 
{
    XMLDocument document;
    document.InsertFirstChild(document.NewDeclaration());
    
    XMLElement* root = document.NewElement("OpenSearchDescription");
    root->SetAttribute("xmlns", "http://a9.com/-/spec/opensearch/1.1/");
    document.InsertEndChild(root);
    
    XMLElement* eName = document.NewElement("ShortName");
    eName->SetText(Config::get("title").c_str());
    root->InsertEndChild(eName);
    
    XMLElement* eDescription = document.NewElement("Description");
    eDescription->SetText(("Recherche d'e-books sur " + Config::get("hostname")).c_str());
    root->InsertEndChild(eDescription);
    
    std::string encoding = "UTF-8";
    XMLElement* iEncoding = document.NewElement("InputEncoding");
    iEncoding->SetText(encoding.c_str());
    root->InsertEndChild(iEncoding);
    
    XMLElement* oEncoding = document.NewElement("OutputEncoding");
    oEncoding->SetText(encoding.c_str());
    root->InsertEndChild(oEncoding);
    
    std::string query = "search/?q={searchTerms}"; 
    XMLElement* urlHtml = document.NewElement("Url");
    urlHtml->SetAttribute("type", "text/html");
    urlHtml->SetAttribute("template", (Config::get("hostname") + query).c_str());
    root->InsertEndChild(urlHtml);
    
    XMLElement* urlXml = document.NewElement("Url");
    urlXml->SetAttribute("type", "text/xml");
    urlXml->SetAttribute("template", (Config::get("hostname") + query + "&out=opds").c_str());
    root->InsertEndChild(urlXml);
    
    document.SaveFile(filename.c_str());
}