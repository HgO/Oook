#include "config.hpp"

std::ostream& operator<<(std::ostream &out, Config const& config) {
    for(auto const& it : config._map)
        out << it.first << " " << it.second << std::endl;
    
    return out;
}