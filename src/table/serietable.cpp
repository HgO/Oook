#include "serietable.hpp"

#include "serie.hpp"

Data* SerieTable::createData(cppdb::result& res) const
{
    std::string name;
    std::string sort;
    
    try {
        name = res.get<std::string>("name");
    } catch(cppdb::invalid_column const& e) {}
    try {
        sort = res.get<std::string>("sort");
    } catch(cppdb::invalid_column const& e) {}
    
    return Serie::create(
        res.get<int>("id"), 
        name,
        sort
    );
}