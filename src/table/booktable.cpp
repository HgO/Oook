#include "booktable.hpp"

#include "search.hpp"
#include "book.hpp"
#include "author.hpp"
#include "tag.hpp"
#include "publisher.hpp"
#include "serie.hpp"
#include "download.hpp"
#include "rating.hpp"
#include "language.hpp"

Data* BookTable::createData(cppdb::result& res) const
{
    std::string title;
    std::string sort;
    int index = 0;
    std::string summary;
    std::string path;
    
    try {
        title = res.get<std::string>("title");
    } catch(cppdb::invalid_column const& e) {}
    try {
        sort = res.get<std::string>("sort");
    } catch(cppdb::invalid_column const& e) {}
    try {
        index = res.get<int>("series_index", 0);
    } catch(cppdb::invalid_column const& e) {}
    try {
        summary = res.get<std::string>("summary", "");
    } catch(cppdb::invalid_column const& e) {}
    try {
        path = res.get<std::string>("path", path);
    } catch(cppdb::invalid_column const& e) {}
    
    Book* book = Book::create(
        res.get<int>("id"), 
        title,
        sort,
        index,
        summary,
        path
    );
    
    try {
        Author* author = Author::create(
            res.get<int>("author_id", 0), 
            res.get<std::string>("author", ""), 
            res.get<std::string>("author_sort", "")
        ); 
        book->addAuthor(author);
        author->addData(book);
    } catch(cppdb::invalid_column const& e) {}
    
    try {
        Rating* rating = Rating::create(
            res.get<int>("rating_id", 0),
            res.get<int>("rating", -1)
        );
        book->setRating(rating);
        rating->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    try {
        Language* language = Language::create(
            res.get<int>("language_id", 0),
            res.get<std::string>("lang_code", "")
        );
        book->addLanguage(language);
        language->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    try {
        Tag* tag = Tag::create(
            res.get<int>("tag_id", 0), 
            res.get<std::string>("tag", "")
        ); 
        book->addTag(tag);
        tag->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    try {
        Publisher* pub = Publisher::create(
            res.get<int>("publisher_id", 0), 
            res.get<std::string>("publisher", ""),
            res.get<std::string>("publisher_sort", "")
        ); 
        book->addPublisher(pub);
        pub->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    try {
        Serie* serie = Serie::create(
            res.get<int>("serie_id", 0), 
            res.get<std::string>("serie", ""),
            res.get<std::string>("serie_sort", "")
        ); 
        book->setSerie(serie);
        serie->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    try {
        Download* format = Download::create(
            res.get<int>("file_id", 0),
            res.get<std::string>("filename", ""),
            res.get<std::string>("format", ""),
            res.get<int>("filesize", 0)
        );
        book->addFormat(format);
        format->addData(book);
    } catch(cppdb::cppdb_error const& e) {}
    
    return book;
}

std::set<Data*> BookTable::fetchAll()
{
    std::vector<std::string> fields{
        "id", 
        "title", 
        "sort"
    };
    
    return Table::fetchAll(fields);
}

Data* BookTable::fetch(int id)
{
    std::vector<std::string> fields{
        "id", 
        "title", 
        "sort", 
        "path",
        "series_index",
        "d.format", 
        "d.id file_id", 
        "d.name filename", 
        "d.uncompressed_size filesize", 
        "c.text summary", 
        "r.rating rating",
        "r.id rating_id",
        "a.name author", 
        "a.id author_id", 
        "a.sort author_sort",
        "k.name tag",
        "k.id tag_id",
        "p.name publisher",
        "p.id publisher_id",
        "p.sort publisher_sort",
        "s.name serie",
        "s.id serie_id",
        "s.sort serie_sort",
        "l.id language_id",
        "l.lang_code lang_code"
    };
    
    std::vector<Join> joins;
    joins.push_back(createJoin("books_authors_link ba", "book", "id"));
    joins.push_back(createJoin("authors a", "id", "author", "ba"));
    
    joins.push_back(createJoin("books_ratings_link br", "book", "id"));
    joins.push_back(createJoin("ratings r", "id", "rating", "br"));
    
    joins.push_back(createJoin("books_tags_link bk", "book", "id"));
    joins.push_back(createJoin("tags k", "id", "tag", "bk"));
    
    joins.push_back(createJoin("books_publishers_link bp", "book", "id"));
    joins.push_back(createJoin("publishers p", "id", "publisher", "bp"));
    
    joins.push_back(createJoin("books_series_link bs", "book", "id"));
    joins.push_back(createJoin("series s", "id", "series", "bs"));
    
    joins.push_back(createJoin("books_languages_link bl", "book", "id"));
    joins.push_back(createJoin("languages l", "id", "lang_code", "bl"));
    
    joins.push_back(createJoin("comments c", "book", "id"));
    joins.push_back(createJoin("data d", "book", "id"));
    
    return Table::fetch(id, fields, joins);
}

std::string BookTable::searchQuery(const std::set< std::string >& query, const std::string& op)
{
    std::vector<std::string> fields{
        "title",
        "c.text"
    };
    
    std::vector<Join> joins;
    joins.push_back(createJoin("comments c", "book", "id"));
    
    return Table::searchQuery(query, op, fields, joins);
}

std::set<Data*> BookTable::search(SearchQuery const& query)
{
    std::vector<std::string> fields{
        "id", 
        "title", 
        "sort", 
        "series_index"
    };
    
    return Table::search(query, fields);
}