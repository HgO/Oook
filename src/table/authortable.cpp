#include "authortable.hpp"

#include "author.hpp"
#include "book.hpp"

Data* AuthorTable::createData(cppdb::result& res) const
{
    std::string name;
    std::string sort;
    
    try {
        name = res.get<std::string>("name");
    } catch(cppdb::invalid_column const& e) {}
    try {
        sort = res.get<std::string>("sort");
    } catch(cppdb::invalid_column const& e) {}
    
    return Author::create(
        res.get<int>("id"),
        name,
        sort
    );
}