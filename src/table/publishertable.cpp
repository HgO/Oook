#include "publishertable.hpp"

#include "publisher.hpp"

Data* PublisherTable::createData(cppdb::result & res) const
{
    std::string name;
    std::string sort;
    
    try {
        name = res.get<std::string>("name");
    } catch(cppdb::invalid_column const& e) {}
    try {
        sort = res.get<std::string>("sort");
    } catch(cppdb::invalid_column const& e) {}
    
    return Publisher::create(
        res.get<int>("id"), 
        name,
        sort
    );
}