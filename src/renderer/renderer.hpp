#ifndef __RENDERER_H__
#define __RENDERER_H__

#include <set>
#include <vector>
#include <string>
#include <map>
#include "config.hpp"
#include "data.hpp"
#include "utils.hpp"

class Data;
class Book;

class Renderer {
public:
    static Renderer& instance() {
        static Renderer instance;
        return instance;
    }
    
    std::string open(const std::string& filename);
    
    std::string pageLink(std::size_t page, std::string const& uri = "");
    std::string pagination(std::size_t page, std::size_t pages, std::string const& uri = "");
    
    void clearCache() {
        _cache.clear();
    }
    
    std::string search(std::string const& query);
    
    std::string header(std::string const& active, std::string const& query = "");
    std::string footer();
protected:
    Renderer() {}
    
protected:
    std::map<std::string, std::string> _cache;
};

#endif