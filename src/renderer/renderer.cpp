#include "renderer.hpp"

#include <fstream>
#include <stdexcept>
#include <cmath>

#include "data.hpp"
#include "publisher.hpp"
#include "book.hpp"
#include "config.hpp"
#include "utils.hpp"

std::string Renderer::header(std::string const& active, std::string const& query)
{
    std::string content = open("header.tpl.html");
    
    Utils::replaceAll(content, "{{title}}", Config::get("title"));
    Utils::replaceAll(content, "{{" + active + "}}", "active");
    Utils::replaceAll(content, "{{search-module}}", search(query));
    
    return content;
}

std::string Renderer::footer()
{
    return open("footer.tpl.html");
}

std::string Renderer::search(std::string const& query) 
{
    std::string content = open("search.tpl.html");
    
    Utils::replaceAll(content, "{{query}}", query);
    
    return content;
}

std::string Renderer::pageLink(std::size_t page, std::string const& uri) {
    std::string link = uri;
    
    if(uri.empty())
        link = (page == 0) ? "." : std::to_string(page) + ".html";
    else if(page != 0)
        link += "&p=" + std::to_string(page);
    
    return link;
}

std::string Renderer::pagination(std::size_t page, std::size_t pages, std::string const& uri) {
    std::string pagination;
    
    if(pages > 1) {
        std::string content;
        
        if(page != 0) {
            std::string first = open("pagination.first.tpl.html");
            
            Utils::replaceAll(first, "{{link}}", (uri.empty()) ? "." : uri);
            
            
            std::string prev = open("pagination.prev.tpl.html");
            
            Utils::replaceAll(prev, "{{link}}", pageLink(page-1, uri));
            
            content = first + prev;
        }
        
        for(std::size_t i = 0; i < pages; ++i) {
            if(i < 2 or i >= pages-2 or (i >= page-1 and i < page+2) or (i == page-2 and i == 2) or (i == page+2 and i == pages-3)) {
                std::string current = open("pagination.link.tpl.html");
                
                Utils::replaceAll(current, "{{link}}", pageLink(i, uri));
                Utils::replaceAll(current, "{{page}}", i+1);
                Utils::replaceAll(current, "{{active}}", (page == i) ? "active" : "");
                
                content += current;
            } else if((i == 3 and page-2 < i) or (i == pages-3 and page+2 > i) or i == page-2 or i == page+2)
                content += open("pagination.hellip.tpl.html");
        }
        
        if(page != pages-1) {
            std::string next = open("pagination.next.tpl.html");
            
            Utils::replaceAll(next, "{{link}}", pageLink(page+1, uri));
            
            std::string last = open("pagination.last.tpl.html");
            
            Utils::replaceAll(last, "{{link}}", pageLink(pages-1, uri));
            
            content += next + last;
        }
        
        pagination = open("pagination.tpl.html");
        
        Utils::replaceAll(pagination, "{{pages}}", content);
    }
    
    return pagination;
}

std::string Renderer::open(const std::string& filename) {
    auto it = _cache.find(filename);
    
    if(it == _cache.end()) {
        std::string path = Config::get("template") + "/" + filename;
        std::ifstream file(path.c_str());
    
        if(!file)
            throw std::runtime_error("Renderer: Can't open template " + path);
    
        std::string content;
        std::string line;
        for(int i = 0; std::getline(file, line); ++i) {
            if(i != 0)
                content += "\n";
            content += line;
        }
        
        file.close();
        
        it = _cache.insert(std::pair<std::string, std::string>(filename, content)).first;   
    }
    
    return it->second;
}