#ifndef __SETUP_H__
#define __SETUP_H__

#include <string>
#include <set>
#include <map>
#include <fstream>

#include "booktable.hpp"
#include "config.hpp"
#include "renderer.hpp"

#include <QProgressDialog>

class Data;
class Feed;
class Dialog;

class Installer {
public:
    Installer(QDialog* parent = nullptr);
    ~Installer() { 
        if(_spinner)
            delete _spinner;
    };
    
    void createDirectory(std::string const& dir) const;
    void createOpensearch(std::string const& filename) const;
    
    void create(const Data* data, const std::string& dir = "", const std::string& pwd = "", Feed const* parent = nullptr);
    void copy(Data const* data, std::string const& dst);
    
protected:
    BookTable _booktable;
    std::size_t _i;
    std::size_t _nformats;
    QProgressDialog* _spinner;
};

#endif