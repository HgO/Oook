#include "mimeutils.hpp"

#include <fstream>
#include <stdexcept>

MimeUtils::MimeUtils() :
    _mimetypes()
{
}
    

MimeUtils::~MimeUtils() 
{
}

std::string MimeUtils::getMimetype(const std::string& extension) const
{
    std::unordered_map<std::string, std::string>::const_iterator it = _mimetypes.find(extension);
    
    if(it != _mimetypes.end())
        return it->second;
    else
        throw std::runtime_error("MimeUtils: Can't find mime type for ." + extension);
}

void MimeUtils::load(const std::string& filename)
{
    std::ifstream ifs(filename);
    
    if(!ifs)
        throw std::runtime_error("MimeUtils: Can't load mime types in " + filename);
    
    while(ifs)
    {
        std::string extension;
        std::string mime;
        
        ifs >> extension >> mime;
        
        MimeUtils::instance()._mimetypes[extension] = mime;
    }
}

void MimeUtils::save(const std::string& filename)
{
    std::ofstream ofs(filename);
    
    if(!ofs)
        throw std::runtime_error("MimeUtils: Can't save mime types in " + filename);
    
    for(auto const& it: MimeUtils::instance()._mimetypes)
        ofs << it.first << " " << it.second << std::endl;
}

void MimeUtils::putMimetype(const std::string& extension, const std::string& mime)
{
    std::unordered_map<std::string, std::string>::iterator it = _mimetypes.find(extension);
    if(it == _mimetypes.end())
        _mimetypes[extension] = mime;
    else if(it->second != mime)
        throw std::runtime_error("MimeUtils: Different mime types for ." + extension + ": " 
                + mime + " vs " + it->second);
}


