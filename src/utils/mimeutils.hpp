#ifndef __MIMEUTILS_H__
#define __MIMEUTILS_H__

#include <unordered_map>
#include <string>

class MimeUtils {
private:
    MimeUtils();
    
    std::string mimetype(const std::string& name, const std::string& extension) const;
public:
    ~MimeUtils();
    
    static MimeUtils &instance() {
        static MimeUtils instance;
        
        return instance;
    }
    
    void operator=(MimeUtils const&) = delete;
    
    static void load(std::string const& filename);
    static void save(std::string const& filename);
    
    std::string getMimetype(std::string const& extension) const;
    static std::string get(std::string const& extension)
    {
        return MimeUtils::instance().getMimetype(extension);
    }
    
    void putMimetype(std::string const& extension, std::string const& mime);
    static void put(std::string const& extension, std::string const& mime) 
    {
        return MimeUtils::instance().putMimetype(extension, mime);
    }
    
private:
    std::unordered_map<std::string, std::string> _mimetypes;
};

#endif