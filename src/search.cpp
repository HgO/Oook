#include "search.hpp"

#include <iostream>
#include <regex>
#include <cmath>

#include "booktable.hpp"
#include "authortable.hpp"
#include "publishertable.hpp"
#include "serietable.hpp"
#include "tagtable.hpp"

#include "data.hpp"
#include "generic.hpp"

#include "feed.hpp"
#include "navigationfeed.hpp"

#include "utils.hpp"
#include "config.hpp"
#include "configparser.hpp"
#include "renderer.hpp"

#include "mimeutils.hpp"

#include <fcgio.h>

Search::Search() :
_tables{new BookTable(), new AuthorTable(), new PublisherTable(), new SerieTable(), new TagTable()}
{
}

Search::~Search() {
    for(Table* table: _tables)
        delete table;
}

Generic* Search::search(const std::string& entry, bool deepSearch) {
    Generic* results = Generic::create("search", "Effectue une recherche dans le catalogue", "Aucun résultat :(");
    results->dataset(std::set<Data*>());
    
    if(!entry.empty()) {
        std::vector<std::string> tokens = tokenize(entry);
        
        if(!tokens.empty()) {
            SearchQuery query = group(tokens);
            
            if(!deepSearch)
            {
                Table* bookTable = _tables[0];
                
                Generic* result = Generic::create("book", "", "Résultat de la recherche pour \"" + entry + "\"");
                result->dataset(bookTable->search(query));
                
                return result;
            }
                
            for(Table* table: _tables)
            {
                std::set<Data*> dataset = table->search(query);
                
                if(!dataset.empty())
                {
                    Generic* result = Generic::create(table->name(), "", Utils::titlize(table->name()));
                    result->dataset(dataset);
                    
                    results->addData(result);
                }
            }
        }   
    }
    
    return results;
}

std::vector<std::string> Search::tokenize(std::string const& entry) {
    std::vector<std::string> tokens;
    
    SearchState state = None, oldState = None;
    
    int start = 0;
    int length = 0;
    for(std::size_t i = 0; i < entry.size(); ++i, ++length) {
        char c = entry[i];
        
        if(c == DoubleQuote and state != BackSlash) {
            if(state == DoubleQuote)
                state = None;
            else
                state = DoubleQuote;
            
            if(length > 0)
                tokens.push_back(entry.substr(start, length));
            
            start = i+1;
            length = -1;
        } else {
            if(state == BackSlash)
                state = oldState;
            
            if(c == None and state == None) {
                if(length > 0)
                    tokens.push_back(entry.substr(start, length));
                
                start = i+1;
                length = -1;
            } else if(c == BackSlash) {
                oldState = state;
                state = BackSlash;
            }
        }
    }
    
    if(length > 0)
        tokens.push_back(entry.substr(start, length));
    
    /* Post-Processing */
        
    for(auto it = tokens.begin(); it != tokens.end(); ++it) {
        if((*it) == "+" or (*it) == "-") {
            std::string token = (*it);
            tokens.erase(it);
            
            if(it != tokens.end())
                (*it) = token + (*it);
            
            --it;
        } else
            Utils::replace((*it), "\\\"", "\"");
    }
    
    return tokens;
}

SearchQuery Search::group(std::vector<std::string> const& tokens) {
    SearchQuery query;
    
    for(std::string const& token : tokens) {
        switch(token[0]) {
            case '+':
                query.ands.insert(token.substr(1, token.size()));
                break;
            case '-':
                query.nots.insert(token.substr(1, token.size()));
                break;
            default:
                query.ors.insert(token);
                break;
        }
    }
    
    return query;
}


int main(int argc, char **argv) {
    ConfigParser().read("oook.conf");
    
    std::streambuf* cin_streambuf  = std::cin.rdbuf();
    std::streambuf* cout_streambuf = std::cout.rdbuf();
    std::streambuf* cerr_streambuf = std::cerr.rdbuf();
    
    FCGX_Init();
    
    FCGX_Request request;
    FCGX_InitRequest(&request,0,0);
    
    Search search;
    std::string oldQuery;
    bool oldIsFeed = false;
    
    std::size_t max = 0, pages = 1;
    Generic* results = nullptr;
    
    MimeUtils::load("../mimetypes.txt");
    
    BookTable bookTable;
    std::set<Data*> books = bookTable.fetchAll();
    for(Data const* book: books)
        bookTable.fetch(book->id());
    while(FCGX_Accept_r(&request) == 0)
    {   
        fcgi_streambuf cin_fcgi_streambuf(request.in);
        fcgi_streambuf cout_fcgi_streambuf(request.out);
        fcgi_streambuf cerr_fcgi_streambuf(request.err);
        
        std::cin.rdbuf(&cin_fcgi_streambuf);
        std::cout.rdbuf(&cout_fcgi_streambuf);
        std::cerr.rdbuf(&cerr_fcgi_streambuf);

        
        std::map<std::string, std::string> params = Utils::paramsURI(
            FCGX_GetParam("REQUEST_URI", request.envp)
        );
        
        std::size_t offset = std::stoi(Config::get("items_per_page"));
        
        std::string query;
        std::size_t page = 0;
        try {
            query = Utils::trim(params.at("q"));
            page = std::stoi(params.at("p"));
        } catch(std::exception const& e) { }
        
        bool isFeed = false;
        try {
            isFeed = (params.at("out") == "opds");
        } catch(std::exception const& e) { }
        
        bool deepSearch = !isFeed;
        
        if((!results and isFeed) or (!query.empty() and (query != oldQuery or oldIsFeed != isFeed)))
        {
            oldQuery = query;
            oldIsFeed = isFeed;
            
            try {
                results = search.search(query, deepSearch);
            } catch(std::exception const& e) {
                std::cerr << e.what() << std::endl;
            }
            
            if(deepSearch)
            {
                max = 0;
                for(Data const* result: results->dataset())
                {
                    std::size_t size = static_cast<Metadata const*>(result)->dataset().size();
                    if(size > max)
                        max = size;
                }
            } else
                max = results->dataset().size();
            
            if(max > 0)
                pages = std::ceil(double(max) / offset);
            else
                pages = 1;
        }
        
        
        if(!isFeed)
        {
            std::cout << "Content-type: text/html" << std::endl << std::endl;
            
            std::string header = Renderer::instance().header("search", query);
            std::string footer = Renderer::instance().footer();
            
            std::string content;
            if(!query.empty() and !results->dataset().empty())
            {
                for(Data const* result: results->dataset())
                    if(offset*page < static_cast<Metadata const*>(result)->dataset().size())
                        content += result->render(page);
            
                std::string pagination = Renderer::instance().pagination(page, pages, "?q=" + query); 
            
                content = pagination + content + pagination;
            } else if(!query.empty())
                content = results->render();
            
            content = header + content + footer;
            
            Utils::replaceAll(content, "{{root}}", "../");
        
            std::cout << content << std::endl;
        } else {
            std::cout << "Content-type: text/xml" << std::endl << std::endl;
            
            if(results)
            {
                Feed* feed = results->createFeed(nullptr, page);
                
                std::string startLink = "?q=" + Utils::encodeURI(query) + "&out=opds";
                
                if(page != 0)
                {
                    feed->setSelfLink(startLink + "&p=" + std::to_string(page));
                    
                    feed->setFirstLink(startLink);
                    if(page-1 != 0)
                        feed->setPrevLink(startLink + "&p=" + std::to_string(page-1));
                    else 
                        feed->setPrevLink(startLink);
                } else
                    feed->setSelfLink(startLink);
                
                if(page < pages-1)
                {
                    feed->setNextLink(startLink + "&p=" + std::to_string(page+1));
                    feed->setLastLink(startLink + "&p=" + std::to_string(pages-1));
                }
                
                std::cout << feed << std::endl;
                
                delete feed;
            }
        }
    }
    
    std::cin.rdbuf(cin_streambuf);
    std::cout.rdbuf(cout_streambuf);
    std::cerr.rdbuf(cerr_streambuf);
    
    return 0;
}
