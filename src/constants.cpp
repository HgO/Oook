#include "constants.hpp"

const std::string SOFTWARE_NAME = "Oook (Open Orang-outan Knowledge)";
const std::string SOFTWARE_VERSION = "1.0.0";
const std::string SOFTWARE_WEBSITE = "http://example.com/";
const std::string SOFTWARE_SEARCH = "search.cgi";