#include "feed.hpp"

#include <ctime>
#include <cstdio>
#include <fstream>

#include "data.hpp"

#include "constants.hpp"
#include "config.hpp"

Feed::Feed(Feed const* parent) :
    _document(), 
    _root(_document.NewElement("feed")),
    _title(_document.NewElement("title")),
    _id(_document.NewElement("id")),
    _selfLink(nullptr),
    _startLink(nullptr),
    _parentLink(nullptr),
    _dir("../")
{
    _document.InsertFirstChild(_document.NewDeclaration());
    
    _root->SetAttribute("xmlns:dcterms", "http://purl.org/dc/terms/");
    _root->SetAttribute("xmlns:opds", "http://opds-spec.org/2010/catalog");
    _root->SetAttribute("xmlns:opensearch", "http://a9.com/-/spec/opensearch/1.1/");
    _root->SetAttribute("xmlns", "http://www.w3.org/2005/Atom");
    
    _document.InsertEndChild(_root);
    _root->InsertEndChild(_id);
    
    addAuthor(SOFTWARE_NAME + " v" + SOFTWARE_VERSION, SOFTWARE_WEBSITE, _root);
    addUpdated(_root);
    
    if(parent) 
    {
        _dir = "../" + parent->_dir;
        
        _startLink = static_cast<XMLElement*>(parent->_startLink->ShallowClone(&_document));
        _startLink->SetAttribute("href", opdsDir().c_str());
        _root->InsertEndChild(_startLink);
        
        _parentLink = static_cast<XMLElement*>(parent->_selfLink->ShallowClone(&_document));
        _parentLink->SetAttribute("rel", "up");
        
        std::string parentUrl = _parentLink->Attribute("href");
        if(parentUrl == ".")
            parentUrl = "../";
        else
            parentUrl = "../" + parentUrl;
        
        _parentLink->SetAttribute("href", parentUrl.c_str());
        _root->InsertEndChild(_parentLink);
    }
    
    addOpensearchLink(_dir + "search/opensearch.xml", "Rechercher", _root);
}

Feed::Feed(const Data* data, Feed const* parent) :
    Feed(parent)
{
    if(data)
    {
        XMLText* textId = _document.NewText(("/" + data->url()).c_str());
        _id->InsertEndChild(textId);
        
        XMLText* textTitle = nullptr;
        if(!data->name().empty()) 
            textTitle = _document.NewText(data->name().c_str());
        else if(!parent)
            textTitle = _document.NewText(("Racine du catalogue " + Config::get("title")).c_str());
        
        if(textTitle)
            _title->InsertEndChild(textTitle);

        _root->InsertFirstChild(_title);
    }
}

XMLElement* Feed::addUpdated(XMLNode* parent)
{
    XMLElement* eUpdated = _document.NewElement("updated");
    parent->InsertEndChild(eUpdated);
    
    std::time_t time = std::time(nullptr);
    std::tm* utc = std::gmtime(&time);
    
    char datetime[sizeof "0000-00-00T00:00:00Z"];
    std::strftime(datetime, sizeof datetime, "%FT%TZ", utc);
    
    XMLText* textUpdated = _document.NewText(datetime);
    eUpdated->InsertEndChild(textUpdated);
    
    return eUpdated;
}

XMLElement* Feed::addCategory(std::string const& name, XMLNode* parent)
{
    XMLElement* eCategory = _document.NewElement("category");
    parent->InsertEndChild(eCategory);
    
    eCategory->SetAttribute("label", name.c_str());
    eCategory->SetAttribute("term", name.c_str());
    
    return eCategory;
}

XMLElement* Feed::addSummary(std::string const& summary, XMLNode* parent)
{
    XMLElement* eSummary = _document.NewElement("content");
    XMLText* textSummary = _document.NewText(summary.c_str());    

    eSummary->SetAttribute("type", "text/html");
    eSummary->InsertEndChild(textSummary);
    parent->InsertEndChild(eSummary);
    
    return eSummary;
}

XMLElement* Feed::addEntry(Data const* data, XMLNode* parent) 
{
    return addEntry(data->name(), "/" + data->url(), data->description(), parent);
}

XMLElement* Feed::addEntry(
    std::string const& title, 
    std::string const& id, 
    std::string const& description, 
    XMLNode* parent)
{
    XMLElement* eEntry = _document.NewElement("entry");
    parent->InsertEndChild(eEntry);
    
    XMLElement* eTitle = _document.NewElement("title");
    XMLText* textTitle = _document.NewText(title.c_str());
    eTitle->InsertEndChild(textTitle);
    eEntry->InsertEndChild(eTitle);
    
    XMLElement* eId = _document.NewElement("id");
    XMLText* textId = _document.NewText(id.c_str());
    eId->InsertEndChild(textId);
    eEntry->InsertEndChild(eId);
    
    addUpdated(eEntry);
    
    if(!description.empty()) {
        XMLElement* eContent = _document.NewElement("content");
        XMLText* textContent = _document.NewText(description.c_str());
        eContent->SetAttribute("type", "text");
        eContent->InsertEndChild(textContent);
        eEntry->InsertEndChild(eContent);
    }

    return eEntry;
}

XMLElement* Feed::addAuthor(std::string const& name, std::string const& uri, XMLNode* parent)
{
    XMLElement* eAuthor = _document.NewElement("author");
    parent->InsertEndChild(eAuthor);
    
    XMLElement* eName = _document.NewElement("name");
    XMLText* textName = _document.NewText(name.c_str());
    eName->InsertEndChild(textName);
    eAuthor->InsertEndChild(eName);
    
    if(!uri.empty())
    {
        XMLElement* eURI = _document.NewElement("uri");
        XMLText* textURI = _document.NewText(uri.c_str());
        eURI->InsertEndChild(textURI);
        eAuthor->InsertEndChild(eURI);
    }
    
    return eAuthor;
}

XMLElement* Feed::addPublisher(std::string const& name, XMLNode* parent)
{
    XMLElement* ePublisher = _document.NewElement("dcterms:publisher");
    XMLText* textPublisher = _document.NewText(name.c_str());
    ePublisher->InsertEndChild(textPublisher);
    parent->InsertEndChild(ePublisher);
    
    return ePublisher;
}

XMLElement* Feed::addLink(
    std::string const& uri, 
    std::string const& title, 
    std::string const& relation,
    std::string const& type,
    XMLNode* parent)
{
    XMLElement* eLink = _document.NewElement("link");
    parent->InsertEndChild(eLink);
    
    eLink->SetAttribute("href", uri.c_str());
    if(!title.empty())
        eLink->SetAttribute("title", title.c_str());
    eLink->SetAttribute("rel", relation.c_str());
    eLink->SetAttribute("type", type.c_str());
    
    return eLink;
}

XMLElement* Feed::addAtomLink(
    std::string const& uri,
    std::string const& title,
    std::string const& relation,
    XMLNode* parent)
{
    XMLElement* eLink = addLink(
        uri, 
        title, 
        relation, 
        "application/atom+xml;profile=opds-catalog;", 
        parent
    );
    
    return eLink;
}

XMLElement* Feed::addNavigationLink(
    std::string const& uri, 
    std::string const& title, 
    std::string const& relation, 
    XMLNode* parent)
{
    XMLElement* eLink = addAtomLink(uri, title, relation, parent);
    
    std::string type = eLink->Attribute("type");
    eLink->SetAttribute("type", (type + "kind=navigation").c_str());
    
    return eLink;
}

XMLElement* Feed::addAcquisitionLink(
    std::string const& uri, 
    std::string const& title, 
    std::string const& relation, 
    XMLNode* parent)
{
    XMLElement* eLink = addAtomLink(uri, title, relation, parent);
    
    std::string type = eLink->Attribute("type");
    eLink->SetAttribute("type", (type + "kind=acquisition").c_str());
    
    return eLink;
}

XMLElement* Feed::addNavigationLink(Data const* data, XMLNode* parent)
{
    return addNavigationLink(opdsDir() + data->url(), data->name(), "subsection", parent);
}

XMLElement* Feed::addAcquisitionLink(Data const* data, XMLNode* parent)
{
    return addAcquisitionLink(opdsDir() + data->url(), data->name(), "subsection", parent);
}

XMLElement* Feed::addOpensearchLink(std::string const& uri, std::string const& title, XMLNode* parent)
{
    return addLink(uri, title, "search", "application/opensearchdescription+xml", parent);
}

XMLElement* Feed::addOpensearch(std::string const& type, std::size_t count, XMLNode* parent)
{
    XMLElement* eOpensearch = _document.NewElement(("opensearch:" + type).c_str());
    XMLText* textOpensearch = _document.NewText(std::to_string(count).c_str());    

    eOpensearch->InsertEndChild(textOpensearch);
    parent->InsertEndChild(eOpensearch);
    
    return eOpensearch;
}

std::ostream & operator<<(std::ostream &out, Feed* feed)
{
    XMLPrinter printer;
    feed->_document.Print(&printer);
    
    out << printer.CStr();
    
    return out;
}
