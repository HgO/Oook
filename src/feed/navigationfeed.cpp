#include "navigationfeed.hpp"

#include "data.hpp"

NavigationFeed::NavigationFeed(const Data* data, std::size_t page, const Feed* parent) : 
    Feed(data, parent)
{
    std::string feedname = ".";
    if(page > 0)
        feedname = std::to_string(page) + ".xml";
    
    _selfLink = addNavigationLink(feedname, "", "self", _root);
    _root->InsertEndChild(_selfLink);
    
    if(!parent) 
    {
        _startLink = addNavigationLink(_selfLink->Attribute("href"), "", "start", _root);
        _root->InsertEndChild(_startLink);   
    }
    
    if(data)
        data->generateFeed(this, page, _root);
}
