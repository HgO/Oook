#include "acquisitionfeed.hpp"

#include "data.hpp"
#include "book.hpp"
#include "download.hpp"
#include "config.hpp"
#include "mimeutils.hpp"

AcquisitionFeed::AcquisitionFeed(const Data* data, std::size_t page, const Feed* parent) : 
    Feed(data, parent)
{
    std::string feedname = ".";
    if(page > 0)
        feedname = std::to_string(page) + ".xml";
    
    _selfLink = addAcquisitionLink(feedname, "", "self", _root);
    _root->InsertEndChild(_selfLink);
    
    if(data)
        data->generateFeed(this, page, _root);
}
