#ifndef __ACQUISITIONFEED_H__
#define __ACQUISITIONFEED_H__

#include "feed.hpp"

class Data;

class AcquisitionFeed : public Feed {
public:
    AcquisitionFeed(Data const* data = nullptr, std::size_t page = 0, Feed const* parent = nullptr);
};

#endif