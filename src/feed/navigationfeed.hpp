#ifndef __NAVIGATIONFEED_H__
#define __NAVIGATIONFEED_H__

#include "feed.hpp"

class Data;

class NavigationFeed : public Feed {
public:
    NavigationFeed(Data const* data = nullptr, std::size_t page = 0, Feed const* parent = nullptr);
};

#endif