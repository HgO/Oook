#ifndef __FEED_H__
#define __FEED_H__

#include <tinyxml2.h>
#include <string>
#include <stdexcept>
#include <iostream>

using namespace tinyxml2;

class Data;

class Feed {
public:
    Feed(Feed const* parent = nullptr);
    Feed(Data const* data, Feed const* parent = nullptr);
    virtual ~Feed() {};
    
    std::string dir() const 
    {
        return _dir;
    }
    
    std::string opdsDir() const 
    {
        return _dir + "opds/";
    }
    
    void setSelfLink(std::string const& name)
    {
        _selfLink->SetAttribute("href", name.c_str());
    }
    
    void setFirstLink(std::string const& name = ".") 
    {
        XMLElement* firstLink = static_cast<XMLElement*>(_selfLink->ShallowClone(&_document));
        
        firstLink->SetAttribute("rel", "first");
        firstLink->SetAttribute("href", name.c_str());
        
        _root->InsertAfterChild(_selfLink, firstLink);
    }
    
    void setPrevLink(std::string const& name = ".") 
    {
        XMLElement* prevLink = static_cast<XMLElement*>(_selfLink->ShallowClone(&_document));
        
        prevLink->SetAttribute("rel", "previous");
        prevLink->SetAttribute("href", name.c_str());
        
        _root->InsertAfterChild(_selfLink, prevLink);
    }
    
    void setNextLink(std::string const& name) 
    {
        XMLElement* nextLink = static_cast<XMLElement*>(_selfLink->ShallowClone(&_document));
        
        nextLink->SetAttribute("rel", "next");
        nextLink->SetAttribute("href", name.c_str());
        
        _root->InsertAfterChild(_selfLink, nextLink);
    }
    
    void setLastLink(std::string const& name) 
    {
        XMLElement* lastLink = static_cast<XMLElement*>(_selfLink->ShallowClone(&_document));
        
        lastLink->SetAttribute("rel", "last");
        lastLink->SetAttribute("href", name.c_str());
        
        _root->InsertAfterChild(_selfLink, lastLink);
    }
    
    void write(std::string const& filename)
    {
        if(_document.SaveFile(filename.c_str()) != XML_SUCCESS)
            throw std::runtime_error("Feed: Can't save feed in " + filename + "."); // :: " + _document.ErrorName());
    }
    
    XMLElement* addUpdated(XMLNode* parent);
    
    XMLElement* addCategory(std::string const& name, XMLNode* parent);
    
    XMLElement* addSummary(std::string const& summary, XMLNode* parent);
    
    XMLElement* addEntry(Data const* data, XMLNode* parent);
    XMLElement* addEntry(
        std::string const& title,
        std::string const& id,
        std::string const& description,
        XMLNode* parent
    );
    
    XMLElement* addAuthor(std::string const& name, std::string const& uri, XMLNode* parent);
    
    XMLElement* addPublisher(std::string const& name, XMLNode* parent);
    
    XMLElement* addLink(
        std::string const& uri, 
        std::string const& title, 
        std::string const& relation, 
        std::string const& type,
        XMLNode* parent
    );
    XMLElement* addAtomLink(
        std::string const& uri, 
        std::string const& title, 
        std::string const& relation, 
        XMLNode* parent
    );
    XMLElement* addNavigationLink(
        std::string const& uri, 
        std::string const& title, 
        std::string const& relation, 
        XMLNode* parent
    );
    XMLElement* addAcquisitionLink(
        std::string const& uri, 
        std::string const& title, 
        std::string const& relation, 
        XMLNode* parent
    );
    XMLElement* addNavigationLink(Data const* data, XMLNode* parent);
    XMLElement* addAcquisitionLink(Data const* data, XMLNode* parent);
    XMLElement* addOpensearchLink(std::string const& uri, std::string const& title, XMLNode* parent);
    
    XMLElement* addOpensearch(std::string const& type, std::size_t count, XMLNode* parent);
    
    friend std::ostream& operator<<(std::ostream &out, Feed* feed);
protected:
    XMLDocument _document;
    XMLElement* _root;
    XMLElement* _title;
    XMLElement* _id;
    XMLElement* _selfLink;
    XMLElement* _startLink;
    XMLElement* _parentLink;
    std::string _dir;
};

#endif
