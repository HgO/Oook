# - Try to find Tinyxml2
# Once done, this will define
#
#  Tinyxml2_FOUND - system has Tinyxml2
#  Tinyxml2_LIBRARIES - link these to use Tinyxml2

include(LibFindMacros)

libfind_pkg_check_modules(Tinyxml2_PKGCONF tinyxml2)

find_library(Tinyxml2_LIBRARY NAMES tinyxml2 PATHS ${Tinyxml2_PKGCONF_LIBRARY_DIRS})

set(Tinyxml2_PROCESS_LIBS Tinyxml2_LIBRARY)

libfind_process(Tinyxml2)