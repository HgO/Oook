# - Try to find Cppdb
# Once done, this will define
#
#  Cppdb_FOUND - system has Cppdb
#  Cppdb_LIBRARIES - link these to use Cppdb

include(LibFindMacros)

libfind_pkg_check_modules(Cppdb_PKGCONF cppdb)

find_library(Cppdb_LIBRARY NAMES cppdb PATHS ${Cppdb_PKGCONF_LIBRARY_DIRS})

set(Cppdb_PROCESS_LIBS Cppdb_LIBRARY)

libfind_process(Cppdb)